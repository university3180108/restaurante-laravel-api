<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlatoController;

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');

Route::get('/platos', [PlatoController::class, 'index']);
Route::get('/platos/{id}', [PlatoController::class, 'show']);
Route::post('/platos', [PlatoController::class, 'store']);
Route::put('/platos/{id}', [PlatoController::class, 'update']);
Route::delete('/platos/{id}', [PlatoController::class, 'destroy']);
