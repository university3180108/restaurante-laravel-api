<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plato extends Model
{
    use HasFactory;

    protected $table = 'plato';
    protected $fillable = [
        'nombre',
        'descripcion',
        'urlImagen',
        'precio',
        'categoria',
        'platosDisponibles',
    ];
}
