<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plato;
use Illuminate\Support\Facades\Validator;

class PlatoController extends Controller
{
    public function index()
    {
        $platos = Plato::all();
        if ($platos->isEmpty()) {
            $data = [
                'message' => 'No se encontraron platos',
                'status' => 404
            ];
            return response()->json($data, 404);
        }
        $data = [
            'platos' => $platos,
            'status' => 200
        ];
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'urlImagen' => 'required',
            'precio' => 'required',
            'categoria' => 'required',
            'platosDisponibles' => 'required',
        ]);

        if ($validator->fails()) {
            $data = [
                'message' => 'Error en la validacion de datos',
                'errors' => $validator->errors(),
                'status' => 400
            ];
            return response()->json($data, 400);
        }

        $plato = Plato::create([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'urlImagen' => $request->urlImagen,
            'precio' => $request->precio,
            'categoria' => $request->categoria,
            'platosDisponibles' => $request->platosDisponibles,
        ]);

        if (!$plato) {
            $data = [
                'message' => 'Error al crear el plato',
                'status' => 500
            ];
            return response()->json($data, 500);
        }

        $data = [
            'plato' => $plato,
            'status' => 201
        ];
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $plato = Plato::find($id);
        if (!$plato) {
            $data = [
                'message' => 'No se encontro el plato con el id',
                'status' => 404
            ];
            return response()->json($data, 404);
        }
        $data = [
            'plato' => $plato,
            'status' => 200
        ];
        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $plato = Plato::find($id);
        if (!$plato) {
            $data = [
                'message' => 'No se encontro el plato con el id',
                'status' => 404
            ];
            return response()->json($data, 404);
        }
        $plato->delete();
        $data = [
            'message' => 'Plato eliminado',
            'status' => 200
        ];
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $plato = Plato::find($id);
        if (!$plato) {
            $data = [
                'message' => 'No se encontro el plato con el id',
                'status' => 404
            ];
            return response()->json($data, 404);
        }

        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'urlImagen' => 'required',
            'precio' => 'required',
            'categoria' => 'required',
            'platosDisponibles' => 'required',
        ]);

        if ($validator->fails()) {
            $data = [
                'message' => 'Error en la validacion de datos',
                'errors' => $validator->errors(),
                'status' => 400
            ];
            return response()->json($data, 400);
        }

        $plato->update(
            $request->only('nombre', 'descripcion', 'urlImagen', 'precio', 'categoria', 'platosDisponibles')
        );
        $data = [
            'plato' => $plato,
            'status' => 200
        ];
        return response()->json($data, 200);
    }
}
